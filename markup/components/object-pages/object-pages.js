/* slider on support-fond block */

let buildSwiper = new Swiper('.swiper-build', {
    loop: true,
    speed: 600,
    noSwiping: true,
    allowTouchMove: false,
    pagination: {
        el: '.swiper-build-pagination',
        type: 'fraction',
    },
    navigation: {
        nextEl: '.swiper-build-next',
        prevEl: '.swiper-build-prev',
    },
});

let buildHistorySwiper = new Swiper('.build__history-swiper', {
    loop: true,
    speed: 600,
    pagination: {
        el: '.build__history-pagination',
        type: 'fraction',
    },
    navigation: {
        nextEl: '.build__history-next ',
        prevEl: '.build__history-prev',
    },
});

