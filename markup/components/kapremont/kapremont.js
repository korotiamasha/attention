var kapSlider = $('.kap-swiper-container');
kapSlider.each(function () {
    var options = $(this).data('options') || {},
        $parent = $(this).parent(),
        swiperDefaults = {
            loop: true,
            speed: 600,
            pagination: {
                el: $parent.find('.kap-pagination')[0],
                type: 'fraction',
            },
            navigation: {
                nextEl: $parent.find('.kap-button-next')[0],
                prevEl: $parent.find('.kap-button-prev')[0],
            }
        };

    var swiperOptions = $.extend(swiperDefaults, options),
        mySwiper = new Swiper(this, swiperOptions);

    mySwiper.on('slideChangeTransitionEnd', function () {
        var text = $parent.find('.swiper-slide-active').attr('data-text')
        $parent.find('.build__slider--info').text(text);
    });
});

$('.build__result--images').twentytwenty({
    'no_overlay': true,
    'click_to_move': true,
});

$('.js-popover').popover({
    trigger: 'hover',
});

$('.kap-menu a').click(function () {
    var dest = $(this).attr('href');
    var top = $(dest).offset().top - 130;
    if (dest !== undefined && dest !== '') {
        $('html').animate({
                scrollTop: top
            }, 1000
        );
    }
    return false;
});

$('body').scrollspy({
    target: '#list-example',
    offset: 150,
});

$('.build__result--images').twentytwenty({
    'no_overlay': true,
    'click_to_move': true,
});
