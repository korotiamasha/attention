let otherObjectsSwiper = new Swiper('.other-obj-swiper', {
    loop: true,
    slidesPerView: 'auto',
    centeredSlides: true,
    spaceBetween: 30,
    noSwiping: true,
    allowTouchMove: false,
    speed: 600,
    navigation: {
        nextEl: '.other-obj-next',
        prevEl: '.other-obj-prev',
    },
});


let safeObjectsSwiper = new Swiper('.safe-obj-swiper', {
    loop: true,
    slidesPerView: 'auto',
    centeredSlides: true,
    spaceBetween: 30,
    noSwiping: true,
    allowTouchMove: false,
    speed: 600,
    navigation: {
        nextEl: '.safe-obj-next',
        prevEl: '.safe-obj-prev',
    },
});
