$(function () {
    $('.js-object-result').twentytwenty({
        'no_overlay': true,
        'click_to_move': true,
    });
    $('.build__result--images').twentytwenty({
        'no_overlay': true,
        'click_to_move': true,
    });
});

/* slider on support-fond block */

let supportSwiper = new Swiper('.swiper-support-fond', {
    loop: true,
    speed: 600,
    pagination: {
        el: '.swiper-support-fond-pagination',
        type: 'fraction',
    },
    navigation: {
        nextEl: '.swiper-support-fond-next',
        prevEl: '.swiper-support-fond-prev',
    },
});

$("#inputPhone").mask("(999) 999 - 99 - 99", {placeholder: "(___) ___ - __ - __ "});

$('.main-banner__item').hover(
    function () {
        if (!($(this).hasClass('active'))) {
            $('.main-banner__item.active').removeClass('active');
            $(this).addClass('active');
            var banner = $('.main-banner');
            var image = $(this).attr('data-image');

            $('img.main-banner__image').remove();
            $('.main-banner__image-wrap').html(' <img src="' + image + '" class="main-banner__image">');

            setTimeout(function () {
                banner.css("background-image", "url(" + image + ")");
            }, 700)
        }
    }, function () {
    }
);
