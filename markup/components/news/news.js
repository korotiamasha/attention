/* slider on news page */

let newsMainSwiper = new Swiper('.news-main-slider', {
    loop: true,
    speed: 600,
    navigation: {
        nextEl: '.news-main-next',
        prevEl: '.news-main-prev',
    },
});
